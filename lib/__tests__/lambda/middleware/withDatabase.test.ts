const mockPgp: any = jest.fn().mockReturnValue('Arbitrary database object');

jest.mock('pg-promise', () => () => mockPgp);

import { withDatabase } from '../../../lambda/middleware/withDatabase';

describe('The withDatabase middleware', () => {
  it('should add the database to the middleware context', () => {
    mockPgp.mockReturnValueOnce('Some database object');

    const enhancedMiddleware = withDatabase('Arbitrary connection string', 'arbitrary-schema', checkAssertions);
    enhancedMiddleware('Arbitrary context');

    function checkAssertions(enhancedContext) {
      expect(enhancedContext.database).toBe('Some database object');

      return Promise.resolve({ arbitrary: 'response' });
    }
  });
});
